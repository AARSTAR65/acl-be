const { createCollection } = require('../services/DB');

const collections = {
  BOOKING_REQUESTS: 'bookingRequests',
  OFFERS: 'offers',
  SERVICE_REVIEWS: 'serviceReviews',
  SERVICES: 'services',
  QUERIES: 'queries',
};

const createCollections = () => {
  (Object.values(collections || {}) || []).forEach((collection) => {
    createCollection(collection);
  });
};

// createCollections();

module.exports = {
  collections,
  createCollections,
};
