const executionHandler = (executionBlock, errorCallback) => {
  try {
    executionBlock();
  } catch (err) {
    errorCallback(err);
  }
};

module.exports = {
  executionHandler,
};
