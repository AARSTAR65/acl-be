var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/';

const DBContextProvider = (executable, reject, DBUrl) => {
  executionHandler(() => {
    MongoClient.connect(DBUrl || url, { useNewUrlParser: true }, (err, db) => {
      if (err) throw err;
      var dbo = db.db('aclDB');
      executable(dbo, db);
      db.close();
    });
  }, reject || (() => {}));
};

const createDB = ({ DBUrl }, resolve, reject) => {
  DBContextProvider(() => (resolve || (() => {}))(), reject, DBUrl);
};

const createCollection = ({ collection }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo.createCollection(collection, (err, response) => {
      if (err) throw err;
      (resolve || (() => {}))(response);
    });
  }, reject);
};

const DBGet = ({ collection, query, projection }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo
      .collection(collection)
      .find(query || {}, { projection: projection || {} })
      .toArray(function (err, response) {
        if (err) throw err;
        (resolve || (() => {}))(response);
      });
  }, reject);
};

const DBGetBatch = ({ collection, query, projection }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo
      .collection(collection)
      .find(query || {}, { projection: projection || {} })
      .toArray(function (err, response) {
        if (err) throw err;
        (resolve || (() => {}))(response);
      });
  }, reject);
};

const DBPost = ({ collection, payload }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo.collection(collection).insertOne(payload, function (err, response) {
      if (err) throw err;
      (resolve || (() => {}))(response);
    });
  }, reject);
};

const DBPostBatch = ({ collection, payload }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo.collection(collection).insertMany(payload, function (err, response) {
      if (err) throw err;
      (resolve || (() => {}))(response);
    });
  }, reject);
};

const DBPatch = ({ collection, payload, query }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo
      .collection(collection)
      .updateOne(query, { $set: payload }, function (err, response) {
        if (err) throw err;
        (resolve || (() => {}))(response);
      });
  }, reject);
};

const DBPatchBatch = ({ collection, payload, query }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo
      .collection(collection)
      .updateMany(query, { $set: payload }, function (err, response) {
        if (err) throw err;
        (resolve || (() => {}))(response);
      });
  }, reject);
};

const DBDelete = ({ collection, query }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo.collection(collection).deleteOne(query, function (err, response) {
      if (err) throw err;
      (resolve || (() => {}))(response);
    });
  }, reject);
};

const DBDeleteBatch = ({ collection, query }, resolve, reject) => {
  DBContextProvider((dbo) => {
    dbo.collection(collection).deleteMany(query, function (err, response) {
      if (err) throw err;
      (resolve || (() => {}))(response);
    });
  }, reject);
};

module.exports = {
  createDB,
  createCollection,
  DBGet,
  DBGetBatch,
  DBPost,
  DBPostBatch,
  DBPatch,
  DBPatchBatch,
  DBDelete,
  DBDeleteBatch,
};
